class UserMailer < ApplicationMailer
  def account_activation(user)
    @user = user
    mail to: user.email, subject: "KATSUOWEBへようこそ！【ユーザー登録】"
  end

  def activation_completed(user)
    @user = user
    mail to: user.email, subject: "【 KATSUOWEB 】ユーザー登録完了"
  end

  def password_reset(user)
    @user = user
    mail to: user.email, subject: "【 KATSUOWEB 】パスワード再設定"
  end

  def reset_completed(user)
    @user = user
    mail to: user.email, subject: "【 KATSUOWEB 】パスワードを変更しました"
  end

  def forward_inquiry(user, mail)
    @user = user
    @mail = mail
    mail to: 'katsuoweb.official@gmail.com',
         cc: 'massadub@gmail.com',
         subject: "【 #{@user.name}さんからの問い合わせ 】#{@mail[:subject]}"
  end

  def forward_reason(user, mail)
    @user = user
    @mail = mail
    mail to: 'katsuoweb.official@gmail.com',
         cc: 'massadub@gmail.com',
         subject: "【 #{@user.name}さんがアカウント削除しました 】"
  end
end
