class ApplicationMailer < ActionMailer::Base
  default from: 'noreply@katsuoweb.com'
  layout 'mailer'
end
