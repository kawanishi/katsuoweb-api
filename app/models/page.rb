class Page < ApplicationRecord
  belongs_to :user

  has_many :likes, dependent: :destroy
  has_many :like_users, through: :likes, source: :user

  mount_base64_uploader :thumbnail, PhotoUploader
  mount_base64_uploader :photo_1, PhotoUploader
  mount_base64_uploader :photo_2, PhotoUploader
  mount_base64_uploader :photo_3, PhotoUploader
  mount_base64_uploader :info_photo, PhotoUploader

  mount_base64_uploader :video, VideoUploader

  validates :user_id, presence: true
  validates :title, presence: true, length: { maximum: 27 }
  validates :description, presence: true
  validates :category, presence: true
  validates :thumbnail, presence: true
  validates :body_1, presence: true

  validate :thumbnail_size
  validate :photo_1_size
  validate :photo_2_size
  validate :photo_3_size
  validate :info_photo_size
  validate :video_size

  def like(user)
    likes.create(user_id: user.id)
  end

  def unlike(user)
    likes.find_by(user_id: user.id).destroy
  end

  def like?(user)
    like_users.include?(user)
  end

  private

    def thumbnail_size
      if thumbnail.size > 5.megabytes
        errors.add(:thumbnail, I18n.t('api.errors.messages.photo_too_big', {count: 5}))
      end
    end

    def video_size
      unless video.nil?
        if video.size > 100.megabytes
          errors.add(:video, I18n.t('api.errors.messages.video_too_big', {count: 100}))
        end
      end
    end

    def photo_1_size
      unless photo_1.nil?
        if photo_1.size > 5.megabytes
          errors.add(:photo_1, I18n.t('api.errors.messages.photo_too_big', {count: 5}))
        end
      end
    end

    def photo_2_size
      unless photo_2.nil?
        if photo_2.size > 5.megabytes
          errors.add(:photo_2, I18n.t('api.errors.messages.photo_too_big', {count: 5}))
        end
      end
    end

    def photo_3_size
      unless photo_3.nil?
        if photo_3.size > 5.megabytes
          errors.add(:photo_3, I18n.t('api.errors.messages.photo_too_big', {count: 5}))
        end
      end
    end

    def info_photo_size
      unless info_photo.nil?
        if info_photo.size > 5.megabytes
          errors.add(:info_photo, I18n.t('api.errors.messages.photo_too_big', {count: 5}))
        end
      end
    end
end
