json.status 200
json.page do
  json.id @page.id
  json.user_id @page.user_id
  json.user_name @page.user.name
  json.user_avatar @page.user.avatar.url
  json.title @page.title
  json.description @page.description
  json.category @page.category.upcase
  json.thumbnail @page.thumbnail.url
  json.latitude @page.latitude
  json.longitude @page.longitude
  json.youtube @page.youtube if @page.youtube
  json.body_1 @page.body_1
  json.body_2 @page.body_2
  json.body_3 @page.body_3
  json.photo_1 @page.photo_1.url
  json.photo_2 @page.photo_2.url
  json.photo_3 @page.photo_3.url
  json.info_title @page.info_title
  json.info_body @page.info_body
  json.info_photo @page.info_photo.url
  json.info_link @page.info_link
  json.created_at convert_time(@page.created_at)
  json.updated_at convert_time(@page.updated_at)
  json.likes_count @page.likes_count
  json.is_liked @page.like?(@current_user)
end
