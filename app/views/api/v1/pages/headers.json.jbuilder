json.status 200
json.headers do
  json.id @page.id
  json.title @page.title
  json.description @page.description
  json.thumbnail @page.thumbnail.url
end
