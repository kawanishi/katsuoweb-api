json.pages do
  json.array!(pages) do |page|
    json.id page.id
    json.user_id page.user_id
    json.user_name page.user.name
    json.user_avatar page.user.avatar.url
    json.title page.title
    json.description page.description
    json.category page.category.upcase
    json.thumbnail page.thumbnail.url
    json.created_at convert_time(page.created_at)
    json.latitude page.latitude
    json.longitude page.longitude
    json.likes_count page.likes_count
  end
end
