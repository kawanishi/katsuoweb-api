json.status 200
json.other_user do
  json.id @user.id
  json.name @user.name
  json.email @user.email
  json.profile @user.profile
  json.avatar @user.avatar.url
  json.activated_at convert_time(@user.activated_at)
  json.is_follow @current_user.following?(@user) if @current_user
  json.is_followed @user.following?(@current_user) if @current_user
end
