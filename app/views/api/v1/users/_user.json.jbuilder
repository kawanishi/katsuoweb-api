json.me do
  json.id user.id
  json.name user.name
  json.email user.email
  json.profile user.profile
  json.avatar user.avatar.url
  json.activated_at convert_time(user.activated_at)
  json.remaining_tickets user.remaining_tickets
end
