json.users do
  json.array!(users) do |user|
    json.id user.id
    json.name user.name
    json.profile user.profile
    json.avatar user.avatar.url
    json.is_follow @current_user.following?(user) if @current_user
    json.is_followed user.following?(@current_user) if @current_user
  end
end
