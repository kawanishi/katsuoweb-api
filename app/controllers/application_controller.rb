class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  private

    def admin_user
      @current_user ||= User.find_by(id: session[:user_id])
      if @current_user && @current_user.admin?
        @admin_user = @current_user
      else
        flash[:warning] = I18n.t('admin.errors.messages.not_admin_user')
        redirect_to admin_sign_in_path
      end
    end
end
