class Api::V1::SessionsController < Api::V1::ApplicationController
  # POST /api/v1/sign_in
  def create
    @user = User.find_by(email: params[:session][:email].downcase)
    if @user && @user.authenticate(params[:session][:password])
      if @user.activated?
        response.headers['Authorization'] = 'Bearer ' + encode_to_jwt(@user, 60)
        head 200
      else
        render json: {
          message: I18n.t('api.errors.messages.not_activated')
        }, status: 401
      end
    else
      render json: {
        message: I18n.t('api.errors.messages.sign_in_failed')
      }, status: 400
    end
  end

  # DELETE /api/v1/sign_out
  def destroy
    begin
      @user ||= User.find_by(id: decode_jwt(request.env['HTTP_AUTHORIZATION'])[0]['user'])
      response.headers['Authorization'] = 'Bearer ' + encode_to_jwt(@user, 0)
      head 200
    rescue JWT::ExpiredSignature
      render json: {
        message: I18n.t('api.errors.messages.sign_in_expired_already')
      }, status: 403
    rescue
      render json: {
        message: I18n.t('api.errors.messages.not_signed_in')
      }, status: 403
    end
  end
end
