class Api::V1::UsersController < Api::V1::ApplicationController
  before_action :current_user, only: [:show, :update, :destroy, :inquiry,
                                      :following, :followers, :other]

  # POST /api/v1/user/sign_up
  def create
    User.transaction do
      user = User.new(user_params)
      user.save!
      user.send_activation_email
    end
    head 200
  rescue => e
    render status: 500, json: { message: e.message }
  end

  # GET /api/v1/user/me
  def show
  end

  # PATCH /api/v1/user/me/profile
  def update
    if @current_user.update_attributes(user_params)
      @user = @current_user
    elsif @current_user.errors.size > 0
      render json: {
        message: @current_user.errors.full_messages.join(' ／ ')
      }, status: 400
    else
      render json: {
        message: I18n.t('api.errors.messages.update_user_failed')
      }, status: 500
    end
  end

  # GET /api/v1/user/activate
  def edit
    @user = User.find_by(email: params[:email])
    if @user && !@user.activated? && @user.authenticated?(:activation, params[:token])
      User.transaction do
        @user.activate
        @user.send_activation_completed_email
      end
      redirect_to api_v1_user_activate_completed_path(format: :html)
    else
      render template: 'api/v1/users/not_completed.html.haml', status: 401
    end
  rescue => e
    render template: 'api/v1/users/not_completed.html.haml', status: 500
  end

  # GET /api/v1/user/all
  def index
    @users
    if params[:page] && params[:page][:number]
      @users = User.where(activated: true)\
                   .paginate(page: params[:page][:number], per_page: 10)
      total_pages = (User.where(activated: true).count / 10.to_f).ceil
      current_page = params[:page][:number].to_i
    else
      @users = User.where(activated: true).limit(10)
      current_page = 1
    end

    response.headers['Pagination'] = {
      current_page: current_page,
      last_page: total_pages,
      next_page_url: "/user/all?page[number]=#{current_page + 1}",
      prev_page_url: "/user/all?page[number]=#{current_page - 1}"
    }.to_json
  end

  # GET /api/v1/users/:id/following
  # following_api_v1_user_path
  def following
    User.transaction do
      @user = User.find(params[:id])
      @users = @user.following
    end
  rescue => e
    render status: 404, json: { message: e.message }
  end

  # GET /api/v1/users/:id/followers
  # followers_api_v1_user_path
  def followers
    User.transaction do
      @user = User.find(params[:id])
      @users = @user.followers
    end
  rescue => e
    render status: 404, json: { message: e.message }
  end

  # GET /api/v1/user/other/:id
  def other
    User.transaction do
      @user = User.find(params[:id])
    end
  rescue => e
    render status: 404, json: { message: e.message }
  end

  # POST /api/v1/user/inquiry
  # api_v1_user_inquiry_path
  def inquiry
    User.transaction do
      @current_user.send_inquiry_email(params[:mail])
    end
    head 200
  rescue => e
    render status: 500, json: { message: e.message }
  end

  # POST /api/v1/user/delete
  # api_v1_user_delete_path
  def destroy
    if @current_user.authenticate(params[:user][:password])
      User.transaction do
        @current_user.destroy
        @current_user.send_reason_email(params[:mail])
      end
      head 200
    else
      render json: {
        message: I18n.t('api.errors.messages.password_not_correct')
      }, status: 400
    end
  rescue => e
    render status: 500, json: { message: e.message }
  end


  private

    def user_params
      params.require(:user).permit(:name, :email, :profile, :avatar, :password, :password_confirmation)
    end
end
