class Api::V1::PasswordsController < Api::V1::ApplicationController
  before_action :get_user, only: [:edit, :update]
  before_action :valid_user, only: [:edit, :update]
  before_action :check_expiration, only: [:edit, :update]

  # POST /api/v1/password/reset
  def create
    @user = User.find_by(email: params[:password][:email].downcase)
    if @user
      User.transaction do
        @user.create_reset_digest
        @user.send_password_reset_email
      end
      head 200
    else
      render json: {
        message: I18n.t('api.errors.messages.email_not_registered')
      }, status: 400
    end
  rescue => e
    render status: 500, json: { message: e.message }
  end

  def edit
  end

  def update
    if params[:user][:password].empty?
      flash[:warning] = I18n.t('api.errors.messages.blank_password')
      redirect_to_edit
    elsif @user.update_attributes(user_params)
      @user.send_password_reset_completed_email
      @user.update_attribute(:reset_digest, nil)
      redirect_to api_v1_password_reset_completed_path(format: :html)
    else
      flash[:warning] = I18n.t('api.errors.messages.invalid_password')
      redirect_to_edit
    end
  end

  private

    def user_params
      params.require(:user).permit(:password, :password_confirmation)
    end

    def get_user
      @user = User.find_by(email: params[:email])
    end

    # 有効なユーザーかどうか確認する
    def valid_user
      unless (@user && @user.activated? &&
              @user.authenticated?(:reset, params[:token]))
        render json: {
          message: I18n.t('api.errors.messages.user_not_exist')
        }, status: 401
      end
    end

    # トークンが期限切れかどうか確認する
    def check_expiration
      if @user.password_reset_expired?
        render json: {
          message: I18n.t('api.errors.messages.password_token_has_expired')
        }, status: 401
      end
    end

    def redirect_to_edit
      redirect_to controller: 'api/v1/passwords', action: 'edit',
        format: 'html', token: params[:token], email: params[:email]
    end
end
