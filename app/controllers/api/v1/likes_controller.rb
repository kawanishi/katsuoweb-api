class Api::V1::LikesController < Api::V1::ApplicationController
  before_action :current_user

  # POST /api/v1/likes
  # api_v1_likes_path
  def create
    if @current_user.remaining_tickets > 0
      Page.transaction do
        page = Page.find(params[:page_id])
        page.like(@current_user) unless page.like?(@current_user)
      end
      remaining = @current_user.remaining_tickets - 1
      User.transaction do
        @current_user.update(remaining_tickets: remaining)
      end
      head 200
    else
      render json: {
        message: I18n.t('api.errors.messages.no_more_tickets')
      }, status: 400
    end
  rescue => e
    render status: 500, json: { message: e.message }
  end

  # DELETE /api/v1/likes/:id
  # api_v1_like_path
  def destroy
    Page.transaction do
      page = Page.find(params[:id])
      page.unlike(@current_user) if page.like?(@current_user)
    end
    remaining = @current_user.remaining_tickets + 1
    User.transaction do
      @current_user.update(remaining_tickets: remaining)
    end
    head 200
  rescue => e
    render status: 500, json: { message: e.message }
  end
end
