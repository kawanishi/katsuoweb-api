class Api::V1::PagesController < Api::V1::ApplicationController
  require 'google/apis/youtube_v3'
  # https://github.com/google/google-api-ruby-client/blob/master/generated/google/apis/youtube_v3/service.rb
  require 'net/http'
  require 'fileutils'
  include Api::V1::PagesHelper

  before_action :current_user, only: [:user_page, :create, :update, :destroy,
                                      :timeline_page, :is_liked, :show, :voted]

  @@temp_page_id = 0

  # GET /api/v1/pages
  # api_v1_pages_path
  def index
    @pages
    if params[:page] && params[:page][:number]
      @pages = Page.paginate(page: params[:page][:number], per_page: 6).order(id: :desc)
      total_pages = (Page.count / 6.to_f).ceil
      current_page = params[:page][:number].to_i
    else
      @pages = Page.all.limit(6).order(id: :desc)
      current_page = 1
    end

    response.headers['Pagination'] = {
      current_page: current_page,
      last_page: total_pages,
      next_page_url: "/pages?page[number]=#{current_page + 1}",
      prev_page_url: "/pages?page[number]=#{current_page - 1}"
    }.to_json
  end

  # GET /api/v1/pages/pickup
  # pickup_api_v1_pages_path
  def pickup
    pages = Page.all.order(id: :desc)
    if !pages.nil? && !pages.empty?
      @pages = pages
    else
      render json: {
        message: I18n.t('api.errors.messages.get_pickup_pages_failed')
      }, status: 404
    end
  end

  # POST /api/v1/pages/search
  # search_api_v1_pages_path
  def search
    pages = Page.where(category: params['categories']).order(id: :desc)
    if !pages.nil? && !pages.empty?
      @pages = pages
    else
      render json: {
        message: I18n.t('api.errors.messages.search_page_not_found')
      }, status: 404
    end
  end

  # GET /api/v1/pages/:id
  # api_v1_page_path
  def show
    unless @page = Page.find_by(id: params[:id])
      render json: {
        message: I18n.t('api.errors.messages.get_page_failed', {id: params[:id]})
      }, status: 404
    end
  end

  # GET /api/v1/pages/:id/headers
  # headers_api_v1_page
  def headers
    unless @page = Page.find_by(id: params[:id])
      render json: {
        message: I18n.t('api.errors.messages.get_page_failed', {id: params[:id]})
      }, status: 404
    end
  end

  # GET /api/v1/pages/:id/user_page
  # user_page_api_v1_page_path
  def user_page
    @pages
    if params[:page] && params[:page][:number]
      @pages = Page.where(user_id: params[:id])\
                   .order(id: :desc)\
                   .paginate(page: params[:page][:number], per_page: 6)
      total_pages = (Page.where(user_id: params[:id]).count / 6.to_f).ceil
      current_page = params[:page][:number].to_i
    else
      if Page.where(user_id: params[:id]).count > 0
        @pages = Page.where(user_id: params[:id]).order(id: :desc).limit(6)
        current_page = 1
      else
        current_page = 0
        total_pages = 0
        head 200
      end
    end

    response.headers['Pagination'] = {
      current_page: current_page,
      last_page: total_pages,
      next_page_url: "/pages/#{params[:id]}/user_page?page[number]=#{current_page + 1}",
      prev_page_url: "/pages/#{params[:id]}/user_page?page[number]=#{current_page - 1}"
    }.to_json
  end

  # GET /api/v1/pages/timeline_page
  # timeline_page_api_v1_pages
  def timeline_page
    @pages
    users = @current_user.following.pluck(:id)
    users.push(@current_user)
    if params[:page] && params[:page][:number]
      @pages = Page.where(user_id: [users])\
                   .paginate(page: params[:page][:number], per_page: 6)\
                   .order(id: :desc)
      total_pages = (Page.where(user_id: [users]).count / 6.to_f).ceil
      current_page = params[:page][:number].to_i
    else
      if Page.where(user_id: [users]).count > 0
        @pages = Page.where(user_id: [users]).order(id: :desc).limit(6)
        current_page = 1
      else
        current_page = 0
        total_pages = 0
        head 200
      end
    end

    response.headers['Pagination'] = {
      current_page: current_page,
      last_page: total_pages,
      next_page_url: "/pages/timeline_page?page[number]=#{current_page + 1}",
      prev_page_url: "/pages/timeline_page?page[number]=#{current_page - 1}"
    }.to_json
  end

  # GET /api/v1/pages/voted
  #  voted_api_v1_pages_path
  def voted
    likes_page = Like.where(user_id: @current_user.id)
    page_ids = likes_page.map { |like| like[:page_id] }
    @pages = Page.where(id: [page_ids]).order(id: :desc)
  end

  # POST /api/v1/pages
  # api_v1_pages_path
  def create
    page = Page.new(page_params)
    if page.save
      render json: { page: { id: page.id } }, status: 200
    elsif page.errors.size > 0
      render json: {
        message: page.errors.full_messages.join(' ／ ')
      }, status: 400
    else
      render json: {
        message: I18n.t('api.errors.messages.create_page_failed')
      }, status: 500
    end
  end

  # PATCH /api/v1/pages/:id
  # api_v1_page_path
  def update
    page = Page.find_by(id: params[:id])
    old_images = get_old_images_before_update(params, page) unless params[:page][:youtube]
    if page.update_attributes(page_params)
      delete_old_images(old_images) if old_images
      render json: { page: { id: page.id } }, status: 200
    elsif page.errors.size > 0
      render json: {
        message: page.errors.full_messages.join(' ／ ')
      }, status: 400
    else
      render json: {
        message: I18n.t('api.errors.messages.update_page_failed')
      }, status: 500
    end
  end

  # PATCH /api/v1/pages/:id/upload_video
  # upload_video_api_v1_page_path
  def upload_video
    page = Page.find_by(id: params[:id])
    @@temp_page_id = params[:id]
    if page.update_attributes(page_params)
      @@temp_page_id = page['id'] if @@temp_page_id === 0
      redirect_url = ''
      if Rails.env.production?
        redirect_url = 'https://accounts.google.com/o/oauth2/auth?' +
          "client_id=#{ENV['GOOGLE_CLIENT_ID']}&" +
          "redirect_uri=https://katsuoweb-api.herokuapp.com/api/v1/pages/callback_auth&" +
          "response_type=code&" +
          "scope=https://www.googleapis.com/auth/youtube"
      else
        redirect_url = 'https://accounts.google.com/o/oauth2/auth?' +
          "client_id=#{ENV['GOOGLE_CLIENT_ID']}&" +
          "redirect_uri=http://localhost:3000/api/v1/pages/callback_auth&" +
          "response_type=code&" +
          "scope=https://www.googleapis.com/auth/youtube"
      end
      render json: { url: redirect_url }, status: 200
    elsif page.errors.size > 0
      render json: {
        message: page.errors.full_messages.join(' ／ ')
      }, status: 400
    else
      render json: {
        message: I18n.t('api.errors.messages.upload_video_failed')
      }, status: 500
    end
  end

  # DELETE /api/v1/pages/:id
  # api_v1_page_path
  def destroy
    page = Page.find_by(id: params[:id])
    if !page['youtube'].nil?
      @@temp_page_id = page['id']
      redirect_url = ''
      if Rails.env.production?
        redirect_url = 'https://accounts.google.com/o/oauth2/auth?' +
          "client_id=#{ENV['GOOGLE_CLIENT_ID']}&" +
          "redirect_uri=https://katsuoweb-api.herokuapp.com/api/v1/pages/callback_auth&" +
          "response_type=code&" +
          "scope=https://www.googleapis.com/auth/youtube"
      else
        redirect_url = 'https://accounts.google.com/o/oauth2/auth?' +
          "client_id=#{ENV['GOOGLE_CLIENT_ID']}&" +
          "redirect_uri=http://localhost:3000/api/v1/pages/callback_auth&" +
          "response_type=code&" +
          "scope=https://www.googleapis.com/auth/youtube"
      end
      render json: { url: redirect_url }, status: 200
    else
      old_images = get_old_images_before_delete(page)
      if page.destroy
        delete_old_images(old_images)
        head 200
      else
        render json: {
          message: I18n.t('api.errors.messages.delete_page_failed')
        }, status: 500
      end
    end
  end

  # GET /api/v1/pages/callback_auth
  # callback_auth_api_v1_pages_path
  def callback_auth
    uri = URI.parse 'https://accounts.google.com/o/oauth2/token'

    post_params = Hash.new
    post_params.store('code', params[:code])
    post_params.store('client_id', ENV['GOOGLE_CLIENT_ID'])
    post_params.store('client_secret', ENV['GOOGLE_CLIENT_SECRETS'])
    if Rails.env.production?
      post_params.store('redirect_uri', 'https://katsuoweb-api.herokuapp.com/api/v1/pages/callback_auth')
    else
      post_params.store('redirect_uri', 'http://localhost:3000/api/v1/pages/callback_auth')
    end
    post_params.store('grant_type', 'authorization_code')

    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE

    http.set_debug_output $stderr

    req = Net::HTTP::Post.new uri.path
    req.set_form_data(post_params)

    access_token = ''

    http.start do |h|
      response = h.request(req)
      result = JSON.parse(response.body)
      access_token = result['access_token']
    end

    connect_youtube(access_token)
  end

  private

    def page_params
      params.require(:page).permit(:user_id, :title, :description, :category,
        :thumbnail, :latitude, :longitude, :video, :youtube, :body_1, :body_2,
        :body_3, :photo_1, :photo_2, :photo_3, :info_title, :info_body,
        :info_photo, :info_link)
    end

    def connect_youtube(access_token)
      authorization = Signet::OAuth2::Client.new(
        access_token: access_token,
        expires_at: Time.current.since(1.hour)
      )

      youtube = Google::Apis::YoutubeV3::YouTubeService.new
      youtube.authorization = authorization

      Page.transaction do
        page = Page.find_by(id: @@temp_page_id)

        part = { snippet: {
            title: page.title,
            description: page.description } }

        youtube.delete_video(page.youtube) if page.youtube

        if File.exist?(Rails.root + "public/uploads/page/video/#{@@temp_page_id}/video.mp4")
          file = File.new(Rails.root + "public/uploads/page/video/#{@@temp_page_id}/video.mp4")
          response = youtube.insert_video('snippet', part, upload_source: file, content_type: 'video/*')
          page.update(youtube: response.id)
          FileUtils.rm_rf(Rails.root + "public/uploads/page/video/#{@@temp_page_id}")
        else
          old_images = get_old_images_before_delete(page)
          page.destroy
          delete_old_images(old_images)
        end
      end

      page = Page.find_by(id: @@temp_page_id)
      if !page.nil?
        if Rails.env.production?
          redirect_to "https://katsuoweb.com/page/#{@@temp_page_id}"
        else
          redirect_to "http://localhost:8080/page/#{@@temp_page_id}"
        end
      else
        if Rails.env.production?
          redirect_to "https://katsuoweb.com/"
        else
          redirect_to "http://localhost:8080/"
        end
      end
    rescue => e
      @message = e.message
      render template: 'api/v1/pages/callback_auth.html.haml', status: 500
    end
end
