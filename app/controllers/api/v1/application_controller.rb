class Api::V1::ApplicationController < ::ActionController::Base
  protect_from_forgery with: :null_session
  include ApplicationHelper

  private

    def current_user
      begin
        @current_user ||= User.find_by(id: decode_jwt(request.env['HTTP_AUTHORIZATION'])[0]['user'])
        response.headers['Authorization'] = 'Bearer ' + encode_to_jwt(@current_user, 60)
      rescue JWT::ExpiredSignature
        render json: {
          message: I18n.t('api.errors.messages.sign_in_has_expired')
        }, status: 403
      rescue
        render json: {
          message: I18n.t('api.errors.messages.not_signed_in')
        }, status: 403
      end
    end
end
