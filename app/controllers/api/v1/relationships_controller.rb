class Api::V1::RelationshipsController < Api::V1::ApplicationController
  before_action :current_user

  # POST /api/v1/relationships
  # api_v1_relationship_path
  def create
    User.transaction do
      user = User.find(params[:followed_id])
      @current_user.follow(user)
    end
    head 200
  rescue => e
    render status: 404, json: { message: e.message }
  end

  # DELETE /api/v1/relationships/:id
  # api_v1_relationship_path
  def destroy
    User.transaction do
      user = User.find(params[:id])
      @current_user.unfollow(user) if @current_user.following?(user)
    end
    head 200
  rescue => e
    render status: 404, json: { message: e.message }
  end
end
