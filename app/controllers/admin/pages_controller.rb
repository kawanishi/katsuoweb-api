class Admin::PagesController < ApplicationController
  before_action :admin_user

  def index
    pages = Page.all.order(id: :desc)
    if !pages.nil? && !pages.empty?
      @pages = pages
    else
      flash[:warning] = I18n.t('admin.errors.messages.pages_not_found')
    end
  end

  def destroy
    if Page.find(params[:id]).destroy
      redirect_to action: :index
    else
      flash[:warning] = I18n.t('admin.errors.messages.delete_page_failed')
      redirect_to action: :index
    end
  end
end
