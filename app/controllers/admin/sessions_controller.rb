class Admin::SessionsController < ApplicationController
  def new
  end

  def create
    @user = User.find_by(email: params[:session][:email].downcase)
    if (@user && @user.authenticate(params[:session][:password]) &&
        @user.activated? && @user.admin?)
      session[:user_id] = @user.id
      redirect_to admin_home_path
    else
      flash[:warning] = I18n.t('admin.errors.messages.not_admin_user')
      render 'new'
    end
  end

  def destroy
    session.delete(:user_id)
    flash[:warning] = I18n.t('admin.success.messages.log_out')
    redirect_to admin_sign_in_path
  end
end
