class Admin::UsersController < ApplicationController
  before_action :admin_user

  def index
    users = User.where.not(id: @admin_user.id)
    if !users.nil? && !users.empty?
      @users = users
    else
      flash[:warning] = I18n.t('admin.errors.messages.users_not_failed')
    end
  end

  def destroy
    if User.find(params[:id]).destroy
      redirect_to action: :index
    else
      flash[:warning] = I18n.t('admin.errors.messages.delete_user_failed')
      redirect_to action: :index
    end
  end
end
