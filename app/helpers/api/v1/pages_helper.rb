module Api::V1::PagesHelper
  def get_old_images_before_update(params, page)
    old_images = []
    temp_array = params[:page][:thumbnail].split('/')
    param_thumbnail = temp_array[4] + '/' + temp_array[5] + '/' + temp_array[6] + '/' + temp_array[7]

    if param_thumbnail != page[:thumbnail]
      old_images.push(get_file_name(page[:thumbnail]))
    end

    if params[:page][:photo_1]
      if page[:photo_1]
        old_images.push(get_file_name(page[:photo_1]))
      end
    end

    if params[:page][:photo_2]
      if page[:photo_2]
        old_images.push(get_file_name(page[:photo_2]))
      end
    end

    if params[:page][:photo_3]
      if page[:photo_3]
        old_images.push(get_file_name(page[:photo_3]))
      end
    end

    if params[:page][:info_photo]
      if page[:info_photo]
        old_images.push(get_file_name(page[:info_photo]))
      end
    end
    old_images
  end

  def get_old_images_before_delete(page)
    old_images = []

    old_images.push(get_file_name(page[:thumbnail]))

    if page[:photo_1]
      old_images.push(get_file_name(page[:photo_1]))
    end

    if page[:photo_2]
      old_images.push(get_file_name(page[:photo_2]))
    end

    if page[:photo_3]
      old_images.push(get_file_name(page[:photo_3]))
    end

    if page[:info_photo]
      old_images.push(get_file_name(page[:info_photo]))
    end
    old_images
  end

  def get_file_name(url)
    temp_array = url.split('/')
    file_name = File.basename(temp_array[3], '.*')
  end

  def delete_old_images(old_images)
    Cloudinary::Api.delete_resources(old_images) unless old_images == []
  end
end
