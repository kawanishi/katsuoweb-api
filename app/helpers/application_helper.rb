module ApplicationHelper
  require 'jwt'

  def encode_to_jwt(user, exp)
    payload = { user: user.id, exp: exp.minutes.from_now.to_i }
    JWT.encode payload, hmac_secret, 'HS256'
  end

  def decode_jwt(token)
    JWT.decode token[7..-1], hmac_secret, true, { algorithm: 'HS256' }
  end

  def hmac_secret
    Rails.application.credentials.config[:secret_key_base]
  end

  def convert_time(time)
    time.strftime('%Y/%m/%d')
  end
end
