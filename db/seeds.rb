User.create!(name:  'KATSUOWEB Official',
             email: 'katsuo@web.com',
             profile: 'KATSUOWEBは、地元に根差した誰からも愛されるお店、ブレない個性を発信し続ける人、地域社会を潤すコミュニティー、知る人ぞ知るとっておきの場所などなど、国内外問わず自分達がグッとくる出来事やモノを紹介していきます。',
             password:              'katsuoweb',
             password_confirmation: 'katsuoweb',
             admin:     true,
             activated: true,
             activated_at: Time.zone.now)

99.times do |n|
  name  = Faker::Name.name
  email = "example-#{n+1}@web.com"
  password = 'password'
  User.create!(name:  name,
               email: email,
               profile: 'KATSUOWEBは、地元に根差した誰からも愛されるお店、ブレない個性を発信し続ける人、地域社会を潤すコミュニティー、知る人ぞ知るとっておきの場所などなど、国内外問わず自分達がグッとくる出来事やモノを紹介していきます。',
               password:              password,
               password_confirmation: password,
               activated: true,
               activated_at: Time.zone.now)
end

users = User.all
user  = users.first
following = users[2..50]
followers = users[3..40]
following.each { |followed| user.follow(followed) }
followers.each { |follower| follower.follow(user) }
