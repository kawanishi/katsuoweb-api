class ChangeAvatarInUser < ActiveRecord::Migration[5.2]
  def change
    change_column_default :users, :avatar, "image/upload/v1537927504/default_avatar.jpg"
  end
end
