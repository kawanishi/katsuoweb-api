class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :name
      t.string :email
      t.text :profile
      t.string :avatar, default: 'image/upload/v1537593645/default_avatar.jpg'
      t.string :password_digest

      t.timestamps
    end
  end
end
