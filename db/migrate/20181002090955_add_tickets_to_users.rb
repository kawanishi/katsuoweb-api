class AddTicketsToUsers < ActiveRecord::Migration[5.2]
  class MigrationUser < ApplicationRecord
    self.table_name = :user
  end

  def up
    _up
  rescue => e
    _down
    raise e
  end

  def down
    _down
  end

  private

    def _up
      MigrationUser.reset_column_information

      add_column :users, :remaining_tickets, :integer, null: false, default: 10 unless column_exists? :users, :remaining_tickets
    end

    def _down
      MigrationUser.reset_column_information

      remove_column :users, :remaining_tickets if column_exists? :users, :remaining_tickets
    end
end
