class CreatePages < ActiveRecord::Migration[5.2]
  def change
    create_table :pages do |t|
      t.references :user, foreign_key: true
      t.string :title
      t.text :description
      t.string :category
      t.string :thumbnail
      t.float :latitude
      t.float :longitude
      t.string :video
      t.string :youtube
      t.text :body_1
      t.text :body_2
      t.text :body_3
      t.string :photo_1
      t.string :photo_2
      t.string :photo_3
      t.string :info_title
      t.text :info_body
      t.string :info_photo
      t.string :info_link

      t.timestamps
    end
    add_index :pages, [:user_id, :created_at]
  end
end
