require 'rails_helper'

RSpec.describe "Sessions API", type: :request do
  describe '#create' do
    let(:user) { User.new(attributes_for(:user)) }
    let(:invalid_email_user) { User.new(attributes_for(:user, :invalid_email)) }
    let(:invalid_password_user) { User.new(attributes_for(:user, :invalid_password)) }

    context 'when success' do
      it "returns headers with JWT and user information" do
        user.save!
        user.activate
        post api_v1_sign_in_path, params: {
          session: { email: user.email, password: user.password } }
        aggregate_failures do
          expect(response).to have_http_status '200'
          expect(response.headers['Authorization']).to match(/Bearer \w+.\w+.\w+/)
        end
      end
    end

    context 'when failure' do
      it "returns error message if email is invalid" do
        user.save!
        user.activate
        post api_v1_sign_in_path, params: {
          session: { email: invalid_email_user.email, password: invalid_email_user.password } }
        aggregate_failures do
          expect(response).to have_http_status '400'
          json = JSON.parse(response.body)
          expect(json['message']).to eq('ログインに失敗しました。メールアドレスかパスワードが間違っています。')
        end
      end

      it "returns error message if pasword is invalid" do
        user.save!
        user.activate
        post api_v1_sign_in_path, params: {
          session: { email: invalid_password_user.email, password: invalid_password_user.password } }
        aggregate_failures do
          expect(response).to have_http_status '400'
          json = JSON.parse(response.body)
          expect(json['message']).to eq('ログインに失敗しました。メールアドレスかパスワードが間違っています。')
        end
      end

      it "returns error message if user is not activated" do
        user.save!
        post api_v1_sign_in_path, params: {
          session: { email: user.email, password: user.password } }
        aggregate_failures do
          expect(response).to have_http_status '401'
          json = JSON.parse(response.body)
          expect(json['message']).to eq(
            'アカウントが認証されていません。ユーザー登録メールを確認してください。')
        end
      end
    end
  end

  describe '#destroy' do
    before do
      @user = User.new(attributes_for(:user))
      @user.save!
      @user.activate
      post api_v1_sign_in_path, params: {
        session: { email: @user.email, password: @user.password } }
      @auth = response.headers['Authorization']
    end

    context 'when success' do
      it 'responds successfully and returns expired Authorization' do
        delete api_v1_sign_out_path, headers: { Authorization: @auth }
        aggregate_failures do
          expect(response).to have_http_status '200'
          auth_after_sign_out = response.headers['Authorization']
          get api_v1_user_me_path, headers: { Authorization: auth_after_sign_out }
          expect(response).to have_http_status '403'
          json = JSON.parse(response.body)
          expect(json['message']).to eq 'ログインの有効期限が切れました。再度ログインしてください。'
        end
      end
    end

    context 'when failure' do
      it 'returns error message if Authorization has been expired', focus: true do
        Timecop.travel(Time.now + 60.minutes)
        delete api_v1_sign_out_path, headers: { Authorization: @auth }
        aggregate_failures do
          expect(response).to have_http_status '403'
          json = JSON.parse(response.body)
          expect(json['message']).to eq '既にログインの有効期限が切れています。'
        end
      end

      it 'returns error message if user did not sign_in' do
        delete api_v1_sign_out_path
        aggregate_failures do
          expect(response).to have_http_status '403'
          json = JSON.parse(response.body)
          expect(json['message']).to eq 'ログインしていません。'
        end
      end
    end
  end
end
