require 'rails_helper'

RSpec.describe "Users API", type: :request do
  let(:mail) { ActionMailer::Base.deliveries.last }

  describe '#create' do
    let(:user_params) { attributes_for(:user) }

    context 'when success' do
      it "responds successfully" do
        expect {
          post api_v1_user_sign_up_path, params: { user: user_params }
        }.to change(User, :count).by(1)
        aggregate_failures do
          expect(response).to have_http_status '200'
          expect(mail).to deliver_to 'test@test.com'
          expect(mail).to have_subject(/KATSUOWEBへようこそ！【ユーザー登録】/)
        end
      end
    end

    context 'when failure' do
      it 'does not save user if creating user in DB is fail' do
        allow_any_instance_of(User).to receive(:save!).and_raise('error')
        expect {
          post api_v1_user_sign_up_path, params: { user: user_params }
        }.to_not change(User, :count)
        aggregate_failures do
          expect(response).to have_http_status '500'
          json = JSON.parse(response.body)
          expect(json['message']).to eq('error')
          expect(mail).to be nil
        end
      end

      it 'does not save user if sending activation email is fail' do
        allow_any_instance_of(User).to receive(:send_activation_email).and_raise('error')
        expect {
          post api_v1_user_sign_up_path, params: { user: user_params }
        }.to_not change(User, :count)
        aggregate_failures do
          expect(response).to have_http_status '500'
          json = JSON.parse(response.body)
          expect(json['message']).to eq('error')
          expect(mail).to be nil
        end
      end
    end
  end

  describe '#edit' do
    let(:user) { User.new(attributes_for(:user)) }
    let(:invalid_email_user) { User.new(attributes_for(:user, :invalid_email)) }

    context 'when success' do
      it 'opens view for activation completed' do
        user.save!
        get api_v1_user_activate_path, params: {
          token: user.activation_token, email: user.email }
        aggregate_failures do
          expect(response).to have_http_status '302'
          expect(response).to redirect_to '/api/v1/user/activate/completed.html'
          @user = User.find_by(email: user.email)
          expect(@user.activated).to eq true
          expect(@user.activated_at).to_not eq nil
          expect(mail).to deliver_to user.email
          expect(mail).to have_subject(/【 KATSUOWEB 】ユーザー登録完了/)
        end
      end
    end

    context 'when failure' do
      it 'opens view for activation not completed if email is invalid' do
        user.save!
        get api_v1_user_activate_path, params: {
          token: user.activation_token, email: invalid_email_user.email }
        aggregate_failures do
          expect(response).to have_http_status '401'
          expect(response).to render_template('api/v1/users/not_completed.html.haml')
          @user = User.find_by(email: user.email)
          expect(@user.activated).to eq false
          expect(@user.activated_at).to eq nil
          expect(mail).to be nil
        end
      end

      it 'opens view for activation not completed if user has been activated' do
        user.save!
        user.activate
        get api_v1_user_activate_path, params: {
          token: user.activation_token, email: user.email }
        aggregate_failures do
          expect(response).to have_http_status '401'
          expect(response).to render_template('api/v1/users/not_completed.html.haml')
          @user = User.find_by(email: user.email)
          expect(@user.activated).to eq true
          expect(@user.activated_at).to_not eq nil
          expect(mail).to be nil
        end
      end

      it 'opens view for activation not completed if user is not authenticated' do
        user.save!
        get api_v1_user_activate_path, params: {
          token: '', email: user.email }
        aggregate_failures do
          expect(response).to have_http_status '401'
          expect(response).to render_template('api/v1/users/not_completed.html.haml')
          @user = User.find_by(email: user.email)
          expect(@user.activated).to eq false
          expect(@user.activated_at).to eq nil
          expect(mail).to be nil
        end
      end

      it 'opens view for activation not completed if user activation is fail' do
        user.save!
        allow_any_instance_of(User).to receive(:activate).and_raise('error')
        get api_v1_user_activate_path, params: {
          token: user.activation_token, email: user.email }
        aggregate_failures do
          expect(response).to have_http_status '500'
          expect(response).to render_template('api/v1/users/not_completed.html.haml')
          @user = User.find_by(email: user.email)
          expect(@user.activated).to eq false
          expect(@user.activated_at).to eq nil
          expect(mail).to be nil
        end
      end

      it 'opens view for activation not completed if sending activation completed email is fail' do
        user.save!
        allow_any_instance_of(User).to receive(:send_activation_completed_email).and_raise('error')
        get api_v1_user_activate_path, params: {
          token: user.activation_token, email: user.email }
        aggregate_failures do
          expect(response).to have_http_status '500'
          expect(response).to render_template('api/v1/users/not_completed.html.haml')
          @user = User.find_by(email: user.email)
          expect(@user.activated).to eq false
          expect(@user.activated_at).to eq nil
          expect(mail).to be nil
        end
      end
    end
  end

  describe '#show' do
    before do
      @user = User.new(attributes_for(:user))
      @user.save!
      @user.activate
      post api_v1_sign_in_path, params: {
        session: { email: @user.email, password: @user.password } }
      @auth = response.headers['Authorization']
    end

    context 'when success' do
      it 'returns user information' do
        get api_v1_user_me_path, headers: { Authorization: @auth }
        aggregate_failures do
          expect(response).to have_http_status '200'
          json = JSON.parse(response.body)
          expect(json['me']['id']).to eq @user.id
          expect(json['me']['name']).to eq @user.name
          expect(json['me']['email']).to eq @user.email
          expect(json['me']['profile']).to eq @user.profile
        end
      end
    end

    context 'when failure' do
      it 'returns error message if user did not sign_in' do
        get api_v1_user_me_path
        aggregate_failures do
          expect(response).to have_http_status '403'
          json = JSON.parse(response.body)
          expect(json['message']).to eq 'ログインしていません。'
        end
      end

      it 'returns error message if Authorization has been expired' do
        Timecop.travel(Time.now + 15.minutes)
        get api_v1_user_me_path, headers: { Authorization: @auth }
        aggregate_failures do
          expect(response).to have_http_status '403'
          json = JSON.parse(response.body)
          expect(json['message']).to eq 'ログインの有効期限が切れました。再度ログインしてください。'
        end
      end
    end
  end

  describe '#update' do
    before do
      @user = User.new(attributes_for(:user))
      @user.save!
      @user.activate
      post api_v1_sign_in_path, params: {
        session: { email: @user.email, password: @user.password } }
      @auth = response.headers['Authorization']
    end

    context 'when success' do
      it 'updates user information' do
        valid_image = 'data:image/jpeg;base64,' + Base64.strict_encode64(File.new(
          Rails.root + 'spec/factories/images/valid_avatar.jpg').read)
        patch api_v1_user_me_profile_path, headers: { Authorization: @auth },
          params: { user: { name: 'UpdateTest', email: @user.email, profile: '更新テスト',
            avatar: valid_image, password: @user.password, password_confirmation: @user.password } }
        aggregate_failures do
          expect(response).to have_http_status '200'
          json = JSON.parse(response.body)
          expect(json['me']['name']).to eq 'UpdateTest'
          expect(json['me']['email']).to eq @user.email
          expect(json['me']['profile']).to eq '更新テスト'
          expect(json['me']['avatar']).to eq '/uploads/avatar.jpeg'
        end
      end
    end

    context 'when failure' do
      it 'returns error message if user did not sign_in' do
        patch api_v1_user_me_profile_path, params: {
          user: { name: 'UpdateTest', email: @user.email, profile: '更新テスト',
            password: @user.password, password_confirmation: @user.password } }
        aggregate_failures do
          expect(response).to have_http_status '403'
          json = JSON.parse(response.body)
          expect(json['message']).to eq 'ログインしていません。'
        end
      end

      it 'returns error message if Authorization has been expired' do
        Timecop.travel(Time.now + 15.minutes)
        patch api_v1_user_me_profile_path, headers: { Authorization: @auth },
          params: { user: { name: 'UpdateTest', email: @user.email, profile: '更新テスト',
            password: @user.password, password_confirmation: @user.password } }
        aggregate_failures do
          expect(response).to have_http_status '403'
          json = JSON.parse(response.body)
          expect(json['message']).to eq 'ログインの有効期限が切れました。再度ログインしてください。'
        end
      end

      it 'returns error message if update of user is fail' do
        allow_any_instance_of(User).to receive(:update_attributes).and_return(false)
        patch api_v1_user_me_profile_path, headers: { Authorization: @auth },
          params: { user: { name: 'UpdateTest', email: @user.email, profile: '更新テスト',
            password: @user.password, password_confirmation: @user.password } }
        aggregate_failures do
          expect(response).to have_http_status '500'
          json = JSON.parse(response.body)
          expect(json['message']).to eq 'ユーザー情報更新に失敗しました。'
        end
      end

      it 'returns error message if avatar size is too big' do
        invalid_image = 'data:image/jpeg;base64,' + Base64.strict_encode64(File.new(
          Rails.root + 'spec/factories/images/invalid_avatar.jpg').read)
        patch api_v1_user_me_profile_path, headers: { Authorization: @auth },
          params: { user: { name: 'UpdateTest', email: @user.email, profile: '更新テスト',
            avatar: invalid_image, password: @user.password, password_confirmation: @user.password } }
        aggregate_failures do
          expect(response).to have_http_status '400'
          json = JSON.parse(response.body)
          expect(json['message']).to eq 'プロフィール画像は、1MB未満にしてください。'
        end
      end
    end
  end

  describe '#index' do
    context 'when success' do
      it 'returns all users' do
        20.times do |n|
          name = Faker::Name.name
          email = "example-#{n+1}@test.com"
          password = 'password'
          User.create!(name: name, email: email,
            password: password, password_confirmation: password)
        end
        get api_v1_user_all_path
        aggregate_failures do
          expect(response).to have_http_status '200'
          json = JSON.parse(response.body)
          expect(json['users'].length).to be 20
          expect(json['users'][0].has_key?('id')).to be true
          expect(json['users'][0].has_key?('name')).to be true
          expect(json['users'][0].has_key?('profile')).to be true
        end
      end
    end

    context 'when failure' do
      it 'returns error message' do
        get api_v1_user_all_path
        aggregate_failures do
          expect(response).to have_http_status '404'
          json = JSON.parse(response.body)
          expect(json['message']).to eq '全ユーザーの取得に失敗しました。'
        end
      end
    end
  end
end
