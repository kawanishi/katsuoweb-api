require 'rails_helper'

RSpec.describe "Pages API", type: :request do
  before(:each) do
    @user = User.new(attributes_for(:user))
    @user.save!
    @user.activate
  end

  after(:each) do
    FileUtils.rm_rf(Rails.root + "public/uploads/")
  end

  describe '#index' do
    context 'when success' do
      it "responds successfully" do
        post api_v1_sign_in_path, params: {
          session: { email: @user.email, password: @user.password } }
        @auth = response.headers['Authorization']
        20.times do |n|
          page_attributes = attributes_for(:page)
          page_attributes[:user_id] = @user.id
          post api_v1_pages_path, params: { page: page_attributes },
            headers: { Authorization: @auth }
        end
        get api_v1_pages_path
        aggregate_failures do
          expect(response).to have_http_status '200'
          json = JSON.parse(response.body)
          expect(json['pages'].length).to be 20
        end
      end
    end

    context 'when failure' do
      it 'returns error message' do
        get api_v1_pages_path
        aggregate_failures do
          expect(response).to have_http_status '404'
          json = JSON.parse(response.body)
          expect(json['message']).to eq '全ページの取得に失敗しました。'
        end
      end
    end
  end

  describe '#search' do
    context 'when success' do
      it 'responds successfully' do
        post api_v1_sign_in_path, params: {
          session: { email: @user.email, password: @user.password } }
        @auth = response.headers['Authorization']
        page_attributes = attributes_for(:page, :category_art)
        page_attributes[:user_id] = @user.id
        post api_v1_pages_path, params: { page: page_attributes },
          headers: { Authorization: @auth }
        post search_api_v1_pages_path, params: { categories: ['art'] }
        aggregate_failures do
          expect(response).to have_http_status '200'
          json = JSON.parse(response.body)
          expect(json['pages'][0]['category']).to eq 'ART'
        end
      end
    end

    context 'when failure' do
      it 'returns error message' do
        post search_api_v1_pages_path, params: { categories: ['art'] }
        aggregate_failures do
          expect(response).to have_http_status '404'
          json = JSON.parse(response.body)
          expect(json['message']).to eq 'ページが見つかりません。'
        end
      end
    end
  end

  describe '#show' do
    context 'when success' do
      it 'responds successfully' do
        post api_v1_sign_in_path, params: {
          session: { email: @user.email, password: @user.password } }
        @auth = response.headers['Authorization']
        page_attributes = attributes_for(:page)
        page_attributes[:user_id] = @user.id
        post api_v1_pages_path, params: { page: page_attributes },
          headers: { Authorization: @auth }
        json = JSON.parse(response.body)
        get api_v1_page_path(json['page']['id'])
        aggregate_failures do
          expect(response).to have_http_status '200'
          json = JSON.parse(response.body)
          expect(json['page']['id']).to eq json['page']['id']
        end
      end
    end

    context 'when failure' do
      it 'returns error message' do
        get api_v1_page_path(255)
        aggregate_failures do
          expect(response).to have_http_status '404'
          json = JSON.parse(response.body)
          expect(json['message']).to eq 'ページ（ID:255）の取得に失敗しました。'
        end
      end
    end
  end

  describe '#user_page' do
    context 'when success' do
      it 'responds successfully' do
        post api_v1_sign_in_path, params: {
          session: { email: @user.email, password: @user.password } }
        @auth = response.headers['Authorization']
        20.times do |n|
          page_attributes = attributes_for(:page)
          page_attributes[:user_id] = @user.id
          post api_v1_pages_path, params: { page: page_attributes },
            headers: { Authorization: @auth }
        end
        get user_page_api_v1_page_path(@user.id),
          headers: { Authorization: @auth }
        aggregate_failures do
          expect(response).to have_http_status '200'
          json = JSON.parse(response.body)
          expect(json['pages'].length).to be 20
          expect(json['pages'][0]['user_name']).to eq @user.name
        end
      end
    end

    context 'when failure' do
      it 'returns error message' do
        post api_v1_sign_in_path, params: {
          session: { email: @user.email, password: @user.password } }
        @auth = response.headers['Authorization']
        get user_page_api_v1_page_path(@user.id),
          headers: { Authorization: @auth }
        aggregate_failures do
          expect(response).to have_http_status '404'
          json = JSON.parse(response.body)
          expect(json['message']).to eq "ユーザー（ID:#{@user.id}）のページは見つかりませんでした。"
        end
      end
    end
  end

  describe '#create' do
    context 'when success without video' do
      it 'responds successfully' do
        post api_v1_sign_in_path, params: {
          session: { email: @user.email, password: @user.password } }
        @auth = response.headers['Authorization']
        page_attributes = attributes_for(:page)
        page_attributes[:user_id] = @user.id
        post api_v1_pages_path, params: { page: page_attributes },
          headers: { Authorization: @auth }
        aggregate_failures do
          expect(response).to have_http_status '200'
          json = JSON.parse(response.body)
          expect(json['page'].has_key?('id')).to be true
        end
      end
    end

    context 'when success with video' do
      it 'responds successfully' do
        post api_v1_sign_in_path, params: {
          session: { email: @user.email, password: @user.password } }
        @auth = response.headers['Authorization']
        page_attributes = attributes_for(:page, :with_video)
        page_attributes[:user_id] = @user.id
        post api_v1_pages_path, params: { page: page_attributes },
          headers: { Authorization: @auth }
        aggregate_failures do
          expect(response).to have_http_status '200'
          json = JSON.parse(response.body)
          expect(json['url']).to eq 'https://accounts.google.com/o/oauth2/auth?' +
            "client_id=#{ENV['GOOGLE_CLIENT_ID']}&" +
            "redirect_uri=http://localhost:3000/api/v1/pages/callback_auth&" +
            "response_type=code&" +
            "scope=https://www.googleapis.com/auth/youtube.upload"
        end
      end
    end

    context 'when failure' do
      it 'returns error message if validation failed' do
        post api_v1_sign_in_path, params: {
          session: { email: @user.email, password: @user.password } }
        @auth = response.headers['Authorization']
        page_attributes = attributes_for(:page, :invalid_title)
        page_attributes[:user_id] = @user.id
        post api_v1_pages_path, params: { page: page_attributes },
          headers: { Authorization: @auth }
        aggregate_failures do
          expect(response).to have_http_status '400'
          json = JSON.parse(response.body)
          expect(json['message']).to eq 'タイトルは27文字以内で入力してください'
        end
      end

      it 'returns error message if saving data to DB is failed' do
        allow_any_instance_of(Page).to receive(:save).and_return(false)
        post api_v1_sign_in_path, params: {
          session: { email: @user.email, password: @user.password } }
        @auth = response.headers['Authorization']
        page_attributes = attributes_for(:page)
        page_attributes[:user_id] = @user.id
        post api_v1_pages_path, params: { page: page_attributes },
          headers: { Authorization: @auth }
        aggregate_failures do
          expect(response).to have_http_status '500'
          json = JSON.parse(response.body)
          expect(json['message']).to eq '新規ページの作成に失敗しました。'
        end
      end
    end
  end

  describe '#update' do
    context 'when success without video' do
      it 'responds successfully' do
        post api_v1_sign_in_path, params: {
          session: { email: @user.email, password: @user.password } }
        @auth = response.headers['Authorization']
        page_attributes = attributes_for(:page)
        page_attributes[:user_id] = @user.id
        post api_v1_pages_path, params: { page: page_attributes },
          headers: { Authorization: @auth }
        json = JSON.parse(response.body)
        update_attributes = attributes_for(:page, :update_title)
        update_attributes[:user_id] = @user.id
        patch api_v1_page_path(json['page']['id']), params: { page: update_attributes },
          headers: { Authorization: @auth }
        expect(response).to have_http_status '200'
        get api_v1_page_path(json['page']['id'])
        aggregate_failures do
          expect(response).to have_http_status '200'
          json = JSON.parse(response.body)
          expect(json['page']['title']).to eq 'UPDATED TITLE'
        end
      end
    end

    context 'when success with video' do
      it 'responds successfully' do
        post api_v1_sign_in_path, params: {
          session: { email: @user.email, password: @user.password } }
        @auth = response.headers['Authorization']
        page_attributes = attributes_for(:page)
        page_attributes[:user_id] = @user.id
        post api_v1_pages_path, params: { page: page_attributes },
          headers: { Authorization: @auth }
        json = JSON.parse(response.body)
        update_attributes = attributes_for(:page, :with_video)
        update_attributes[:user_id] = @user.id
        patch api_v1_page_path(json['page']['id']), params: { page: update_attributes },
          headers: { Authorization: @auth }
        aggregate_failures do
          expect(response).to have_http_status '200'
          json = JSON.parse(response.body)
          expect(json['url']).to eq 'https://accounts.google.com/o/oauth2/auth?' +
            "client_id=#{ENV['GOOGLE_CLIENT_ID']}&" +
            "redirect_uri=http://localhost:3000/api/v1/pages/callback_auth&" +
            "response_type=code&" +
            "scope=https://www.googleapis.com/auth/youtube"
        end
      end
    end

    context 'when failure' do
      it 'returns error message if validation failed' do
        post api_v1_sign_in_path, params: {
          session: { email: @user.email, password: @user.password } }
        @auth = response.headers['Authorization']
        page_attributes = attributes_for(:page)
        page_attributes[:user_id] = @user.id
        post api_v1_pages_path, params: { page: page_attributes },
          headers: { Authorization: @auth }
        json = JSON.parse(response.body)
        update_attributes = attributes_for(:page, :invalid_title)
        update_attributes[:user_id] = @user.id
        patch api_v1_page_path(json['page']['id']), params: { page: update_attributes },
          headers: { Authorization: @auth }
        aggregate_failures do
          expect(response).to have_http_status '400'
          json = JSON.parse(response.body)
          expect(json['message']).to eq 'タイトルは27文字以内で入力してください'
        end
      end

      it 'returns error message if saving data to DB is failed' do
        allow_any_instance_of(Page).to receive(:update_attributes).and_return(false)
        post api_v1_sign_in_path, params: {
          session: { email: @user.email, password: @user.password } }
        @auth = response.headers['Authorization']
        page_attributes = attributes_for(:page)
        page_attributes[:user_id] = @user.id
        post api_v1_pages_path, params: { page: page_attributes },
          headers: { Authorization: @auth }
        json = JSON.parse(response.body)
        update_attributes = attributes_for(:page, :update_title)
        update_attributes[:user_id] = @user.id
        patch api_v1_page_path(json['page']['id']), params: { page: update_attributes },
          headers: { Authorization: @auth }
        aggregate_failures do
          expect(response).to have_http_status '500'
          json = JSON.parse(response.body)
          expect(json['message']).to eq 'ページの更新に失敗しました。'
        end
      end
    end
  end

  describe '#destroy' do
    context 'when success' do
      it 'responds successfully if page has not youtube' do
        post api_v1_sign_in_path, params: {
          session: { email: @user.email, password: @user.password } }
        @auth = response.headers['Authorization']
        page_attributes = attributes_for(:page)
        page_attributes[:user_id] = @user.id
        post api_v1_pages_path, params: { page: page_attributes },
          headers: { Authorization: @auth }
        json = JSON.parse(response.body)
        delete api_v1_page_path(json['page']['id']),
          headers: { Authorization: @auth }
        expect(response).to have_http_status '200'
        get api_v1_page_path(json['page']['id'])
        aggregate_failures do
          expect(response).to have_http_status '404'
          result = JSON.parse(response.body)
          expect(result['message']).to eq "ページ（ID:#{json['page']['id']}）の取得に失敗しました。"
        end
      end

      it 'responds successfully if page has youtube' do
        post api_v1_sign_in_path, params: {
          session: { email: @user.email, password: @user.password } }
        @auth = response.headers['Authorization']
        page_attributes = attributes_for(:page, :with_youtube)
        page_attributes[:user_id] = @user.id
        post api_v1_pages_path, params: { page: page_attributes },
          headers: { Authorization: @auth }
        json = JSON.parse(response.body)
        delete api_v1_page_path(json['page']['id']),
          headers: { Authorization: @auth }
        aggregate_failures do
          expect(response).to have_http_status '200'
          json = JSON.parse(response.body)
          expect(json['url']).to eq 'https://accounts.google.com/o/oauth2/auth?' +
            "client_id=#{ENV['GOOGLE_CLIENT_ID']}&" +
            "redirect_uri=http://localhost:3000/api/v1/pages/callback_auth&" +
            "response_type=code&" +
            "scope=https://www.googleapis.com/auth/youtube"
        end
      end
    end

    context 'when failure' do
      it 'returns error message' do
        allow_any_instance_of(Page).to receive(:destroy).and_return(false)
        post api_v1_sign_in_path, params: {
          session: { email: @user.email, password: @user.password } }
        @auth = response.headers['Authorization']
        page_attributes = attributes_for(:page)
        page_attributes[:user_id] = @user.id
        post api_v1_pages_path, params: { page: page_attributes },
          headers: { Authorization: @auth }
        json = JSON.parse(response.body)
        delete api_v1_page_path(json['page']['id']),
          headers: { Authorization: @auth }
        aggregate_failures do
          expect(response).to have_http_status '500'
          result = JSON.parse(response.body)
          expect(result['message']).to eq 'ページの削除に失敗しました。'
        end
      end
    end
  end
end
