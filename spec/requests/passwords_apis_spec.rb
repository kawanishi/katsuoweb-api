require 'rails_helper'

RSpec.describe "Passwords API", type: :request do
  let(:mail) { ActionMailer::Base.deliveries.last }

  describe '#create' do
    before do
      @user = User.new(attributes_for(:user))
      @user.save!
      @user.activate
    end

    context 'when success' do
      it 'responds successfully' do
        post api_v1_password_reset_path, params: { password: { email: @user.email } }
        aggregate_failures do
          expect(response).to have_http_status '200'
          expect(mail).to deliver_to @user.email
          expect(mail).to have_subject(/【 KATSUOWEB 】パスワード再設定/)
        end
      end
    end

    context 'when failure' do
      it 'returns error message if email is invalid' do
        post api_v1_password_reset_path, params: { password: { email: '' } }
        aggregate_failures do
          expect(response).to have_http_status '400'
          json = JSON.parse(response.body)
          expect(json['message']).to eq('メールアドレスが登録されていません。')
          expect(mail).to be nil
        end
      end

      it 'returns error message if creating reset diget is fail' do
        allow_any_instance_of(User).to receive(:create_reset_digest).and_raise('error')
        post api_v1_password_reset_path, params: { password: { email: @user.email } }
        aggregate_failures do
          expect(response).to have_http_status '500'
          json = JSON.parse(response.body)
          expect(json['message']).to eq('error')
          expect(mail).to be nil
        end
      end

      it 'returns error message if sending password reset mail is fail' do
        allow_any_instance_of(User).to receive(:send_password_reset_email).and_raise('error')
        post api_v1_password_reset_path, params: { password: { email: @user.email } }
        aggregate_failures do
          expect(response).to have_http_status '500'
          json = JSON.parse(response.body)
          expect(json['message']).to eq('error')
          expect(mail).to be nil
        end
      end
    end
  end
end
