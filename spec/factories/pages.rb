FactoryBot.define do
  factory :page do
    title Faker::Coffee.blend_name
    description Faker::Hipster.sentence
    category ['market', 'event', 'music', 'cooking', 'shop', 'travel',
      'lifestyle', 'art', 'culture', 'others'].sample
    thumbnail 'data:image/jpeg;base64,' + Base64.strict_encode64(File.new(
      Rails.root + 'spec/factories/images/sample_thumbnail.jpg').read)
    body_1 Faker::Hipster.paragraph

    trait :invalid_title do
      title 'a' * 28
    end

    trait :category_art do
      category 'art'
    end

    trait :with_video do
      video 'data:video/mp4;base64,' + Base64.strict_encode64(File.new(
        Rails.root + 'spec/factories/videos/sample_video.mp4').read)
    end

    trait :update_title do
      title 'UPDATED TITLE'
    end

    trait :with_youtube do
      youtube 'TEST'
    end
  end
end
