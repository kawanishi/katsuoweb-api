FactoryBot.define do
  factory :user do
    name 'Test'
    email 'test@test.com'
    password 'password'
    password_confirmation 'password'

    trait :invalid_email do
      email 'invalid@test.com'
    end

    trait :invalid_password do
      password 'invalid_password'
    end

    trait :admin_user do
      name '管理者'
      email 'admin@admin.com'
      password 'adminadmin'
      password_confirmation 'adminadmin'
    end

    trait :delete_user do
      name '削除ユーザー'
      email 'delete@test.com'
    end

    trait :random_user do
      name 'ランダムユーザー'
      email 'random@test.com'
    end
  end
end
