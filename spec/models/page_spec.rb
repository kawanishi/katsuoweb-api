require 'rails_helper'

RSpec.describe Page, type: :model do
  before(:each) do
    @user = User.new(attributes_for(:user))
    @user.save!
    @user.activate
    @page_attributes = attributes_for(:page)
    @page_attributes[:user_id] = @user.id
  end

  after(:each) do
    FileUtils.rm_rf(Rails.root + "public/uploads/")
  end

  describe 'validation' do
    it 'is valid with all required attributes' do
      page = Page.create(@page_attributes)
      expect(page).to be_valid
    end

    context 'validation for user_id' do
      it 'is invalid without user_id' do
        @page_attributes[:user_id] = nil
        page = Page.create(@page_attributes)
        page.valid?
        expect(page.errors[:user_id]).to eq I18n.t(['errors.messages.blank'])
      end

      it 'is invalid if user_id is not exist' do
        @page_attributes[:user_id] = @user.id + 1
        page = Page.create(@page_attributes)
        expect(page).to_not be_valid
      end
    end

    context 'validation for title' do
      it 'is invalid without title' do
        @page_attributes[:title] = nil
        page = Page.create(@page_attributes)
        page.valid?
        expect(page.errors[:title]).to eq I18n.t(['errors.messages.blank'])
      end

      it 'is valid if length of title is 27' do
        @page_attributes[:title] = 'a' * 27
        page = Page.create(@page_attributes)
        expect(page).to be_valid
      end

      it 'is invalid if length of title is more than 27' do
        @page_attributes[:title] = 'a' * 28
        page = Page.create(@page_attributes)
        page.valid?
        expect(page.errors[:title]).to eq I18n.t(
          ['errors.messages.too_long'], {count: 27})
      end
    end

    context 'validation for description' do
      it 'is invalid without description' do
        @page_attributes[:description] = nil
        page = Page.create(@page_attributes)
        page.valid?
        expect(page.errors[:description]).to eq I18n.t(['errors.messages.blank'])
      end
    end

    context 'validation for category' do
      it 'is invalid without category' do
        @page_attributes[:category] = nil
        page = Page.create(@page_attributes)
        page.valid?
        expect(page.errors[:category]).to eq I18n.t(['errors.messages.blank'])
      end
    end

    context 'validation for thumbnail' do
      it 'is invalid without thumbnail' do
        @page_attributes[:thumbnail] = nil
        page = Page.create(@page_attributes)
        page.valid?
        expect(page.errors[:thumbnail]).to eq I18n.t(['errors.messages.blank'])
      end

      it 'is invalid if size of thumbnail is more than 1MB' do
        @page_attributes[:thumbnail] = 'data:image/jpeg;base64,' + Base64.strict_encode64(File.new(
          Rails.root + 'spec/factories/images/over_1MB_photo.jpg').read)
        page = Page.create(@page_attributes)
        page.valid?
        expect(page.errors[:thumbnail]).to eq I18n.t(['api.errors.messages.photo_too_big'], {count: 1})
      end
    end

    context 'validation for body_1' do
      it 'is invalid without body_1' do
        @page_attributes[:body_1] = nil
        page = Page.create(@page_attributes)
        page.valid?
        expect(page.errors[:body_1]).to eq I18n.t(['errors.messages.blank'])
      end
    end

    context 'validation for photo_1' do
      it 'is invalid if size of photo_1 is more than 1MB' do
        @page_attributes[:photo_1] = 'data:image/jpeg;base64,' + Base64.strict_encode64(File.new(
          Rails.root + 'spec/factories/images/over_1MB_photo.jpg').read)
        page = Page.create(@page_attributes)
        page.valid?
        expect(page.errors[:photo_1]).to eq I18n.t(['api.errors.messages.photo_too_big'], {count: 1})
      end
    end

    context 'validation for photo_2' do
      it 'is invalid if size of photo_2 is more than 1MB' do
        @page_attributes[:photo_2] = 'data:image/jpeg;base64,' + Base64.strict_encode64(File.new(
          Rails.root + 'spec/factories/images/over_1MB_photo.jpg').read)
        page = Page.create(@page_attributes)
        page.valid?
        expect(page.errors[:photo_2]).to eq I18n.t(['api.errors.messages.photo_too_big'], {count: 1})
      end
    end

    context 'validation for photo_3' do
      it 'is invalid if size of photo_3 is more than 1MB' do
        @page_attributes[:photo_3] = 'data:image/jpeg;base64,' + Base64.strict_encode64(File.new(
          Rails.root + 'spec/factories/images/over_1MB_photo.jpg').read)
        page = Page.create(@page_attributes)
        page.valid?
        expect(page.errors[:photo_3]).to eq I18n.t(['api.errors.messages.photo_too_big'], {count: 1})
      end
    end

    context 'validation for info_photo' do
      it 'is invalid if size of info_photo is more than 1MB' do
        @page_attributes[:info_photo] = 'data:image/jpeg;base64,' + Base64.strict_encode64(File.new(
          Rails.root + 'spec/factories/images/over_1MB_photo.jpg').read)
        page = Page.create(@page_attributes)
        page.valid?
        expect(page.errors[:info_photo]).to eq I18n.t(['api.errors.messages.photo_too_big'], {count: 1})
      end
    end

    context 'validation for video' do
      it 'is invalid if size of video is more than 100MB' do
        @page_attributes[:video] = 'data:video/mp4;base64,' + Base64.strict_encode64(File.new(
          Rails.root + 'spec/factories/videos/over_100MB_video.MP4').read)
        page = Page.create(@page_attributes)
        page.valid?
        expect(page.errors[:video]).to eq I18n.t(['api.errors.messages.video_too_big'], {count: 100})
      end
    end
  end
end
