require 'rails_helper'

RSpec.describe User, type: :model do
  before(:each) do
    @user = create(:user)
  end

  describe 'validation' do
    it 'is valid with name, email, and password' do
      expect(@user).to be_valid
    end

    describe 'validation for name' do
      it 'is invalid without name' do
        @user.name = nil
        @user.valid?
        expect(@user.errors[:name]).to eq I18n.t(['errors.messages.blank'])
      end

      it 'is valid if length of name is 50' do
        @user.name = 'a' * 50
        expect(@user).to be_valid
      end

      it 'is invalid if length of name is more than 50' do
        @user.name = 'a' * 51
        @user.valid?
        expect(@user.errors[:name]).to eq I18n.t(
          ['errors.messages.too_long'], {count: 50})
      end
    end

    describe 'validation for email' do
      it 'is invalid without email' do
        @user.email = nil
        @user.valid?
        expect(@user.errors[:email]).to eq I18n.t(
          ['errors.messages.blank', 'errors.messages.invalid'])
      end

      it 'is valid if length of email is 255' do
        @user.email = 'a' * 246 + '@test.com'
        expect(@user).to be_valid
      end

      it 'is invalid if length of email is more than 255' do
        @user.email = 'a' * 247 + '@test.com'
        @user.valid?
        expect(@user.errors[:email]).to eq I18n.t(
          ['errors.messages.too_long'], {count: 255})
      end

      it 'is invalid if email does not match with regex' do
        @user.email = 'test@test,com'
        @user.valid?
        expect(@user.errors[:email]).to eq I18n.t(['errors.messages.invalid'])
      end

      it 'is invalid with a duplicate email' do
        new_user = User.new(name: 'Test', email: 'test@test.com', profile: '',
          password: 'password', password_confirmation: 'password')
        new_user.valid?
        expect(new_user.errors[:email]).to eq I18n.t(['errors.messages.taken'])
      end
    end

    describe 'validation for password' do
      it 'is invalid without password' do
        @user.password = @user.password_confirmation = nil
        @user.valid?
        expect(@user.errors[:password]).to eq I18n.t(['errors.messages.blank'])
      end

      it 'is invalid if length of password is less than 8' do
        @user.password = @user.password_confirmation = 'a' * 7
        @user.valid?
        expect(@user.errors[:password]).to eq I18n.t(
          ['errors.messages.too_short'], {count: 8})
      end

      it 'is invalid if password and password_comfirmation are different' do
        @user.password_confirmation = ''
        @user.valid?
        expect(@user.errors[:password_confirmation]).to eq I18n.t(
          ['errors.messages.confirmation'],
          {attribute: I18n.t('activerecord.attributes.user.password')})
      end
    end
  end
end
