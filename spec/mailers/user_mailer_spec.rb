require "rails_helper"

RSpec.describe UserMailer, type: :mailer do
  let(:user) { User.new(attributes_for(:user)) }
  let(:mail) { ActionMailer::Base.deliveries.last }

  describe 'account_activation' do
    before do
      user.send_activation_email
    end

    it "sends a account activation email to the user's email address" do
      expect(mail).to deliver_to user.email
    end

    it 'sends from the default email address' do
      expect(mail).to deliver_from('noreply@katsuoweb.com')
    end

    it 'sends with the correct subject' do
      expect(mail).to have_subject(/KATSUOWEBへようこそ！【ユーザー登録】/)
    end

    it "contains user's name in the mail body" do
      expect(mail).to have_body_text(/#{user.name}/)
    end

    it 'contains activation link in the mail body' do
      expect(mail).to have_body_text(/#{api_v1_user_activate_path}/)
    end
  end

  describe 'activation_completed' do
    before do
      user.send_activation_completed_email
    end

    it "sends a activation completed email to the user's email address" do
      expect(mail).to deliver_to user.email
    end

    it 'sends from the default email address' do
      expect(mail).to deliver_from('noreply@katsuoweb.com')
    end

    it 'sends with the correct subject' do
      expect(mail).to have_subject(/【 KATSUOWEB 】ユーザー登録完了/)
    end

    it "contains user's name in the mail body" do
      expect(mail).to have_body_text(/#{user.name}/)
    end

    it "contains the sentence in the mail body" do
      expect(mail).to have_body_text('下記の内容で、ユーザー登録が完了しました。')
    end
  end

  describe 'password_reset' do
    before do
      user.send_password_reset_email
    end

    it "sends a password reset email to the user's email address" do
      expect(mail).to deliver_to user.email
    end

    it 'sends from the default email address' do
      expect(mail).to deliver_from('noreply@katsuoweb.com')
    end

    it 'sends with the correct subject' do
      expect(mail).to have_subject(/【 KATSUOWEB 】パスワード再設定/)
    end

    it "contains the sentence in the mail body" do
      expect(mail).to have_body_text('パスワードを再設定するために、'\
        '下記のリンクをクリックしてください。')
    end

    it 'contains password reset link in the mail body' do
      expect(mail).to have_body_text(/#{api_v1_password_reset_path}/)
    end
  end

  describe 'reset_completed' do
    before do
      user.send_password_reset_completed_email
    end

    it "sends a password reset email to the user's email address" do
      expect(mail).to deliver_to user.email
    end

    it 'sends from the default email address' do
      expect(mail).to deliver_from('noreply@katsuoweb.com')
    end

    it 'sends with the correct subject' do
      expect(mail).to have_subject(/【 KATSUOWEB 】パスワードを変更しました/)
    end

    it "contains user's name in the mail body" do
      expect(mail).to have_body_text(/#{user.name}/)
    end

    it "contains the sentence in the mail body" do
      expect(mail).to have_body_text('パスワードの変更が完了しました。')
    end
  end
end
