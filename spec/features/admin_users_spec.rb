require 'rails_helper'

RSpec.feature "AdminUsers", type: :feature do
  before do
    @user = create(:user, :admin_user)
    @user.save!
    @user.activate
    @user.update_attributes(admin: true)
  end

  scenario 'all not admin users are displayed' do
    create(:user)
    visit admin_sign_in_path
    fill_in 'Email', with: @user.email
    fill_in 'Password', with: @user.password
    click_button 'ログイン'
    click_link 'ユーザーマスター'
    aggregate_failures do
      expect(page).to have_current_path admin_users_path
      expect(page).to have_content 'ユーザーマスター'
      expect(page).to_not have_content '管理者'
      expect(page).to have_content 'Test'
    end
  end

  scenario 'it should display warning message if any users not exist' do
    visit admin_sign_in_path
    fill_in 'Email', with: @user.email
    fill_in 'Password', with: @user.password
    click_button 'ログイン'
    click_link 'ユーザーマスター'
    aggregate_failures do
      expect(page).to have_current_path admin_users_path
      expect(page).to have_content 'ユーザーが見つかりません。'
    end
  end

  scenario 'admin user can delete users' do
    create(:user, :delete_user)
    create(:user, :random_user)
    visit admin_sign_in_path
    fill_in 'Email', with: @user.email
    fill_in 'Password', with: @user.password
    click_button 'ログイン'
    click_link 'ユーザーマスター'
    click_link '削除', match: :first
    aggregate_failures do
      expect(page).to have_current_path admin_users_path
      expect(page).to_not have_content '削除ユーザー'
    end
  end

  scenario 'error message should show if delete user fail' do
    create(:user, :delete_user)
    create(:user, :random_user)
    allow_any_instance_of(User).to receive(:destroy).and_return(false)
    visit admin_sign_in_path
    fill_in 'Email', with: @user.email
    fill_in 'Password', with: @user.password
    click_button 'ログイン'
    click_link 'ユーザーマスター'
    click_link '削除', match: :first
    aggregate_failures do
      expect(page).to have_current_path admin_users_path
      expect(page).to have_content 'ユーザーの削除に失敗しました。'
      expect(page).to have_content '削除ユーザー'
    end
  end
end
