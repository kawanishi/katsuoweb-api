require 'rails_helper'

RSpec.feature "Passwords", type: :feature do
  let(:mail) { ActionMailer::Base.deliveries.last }

  before do
    @user = User.new(attributes_for(:user))
    @user.save!
    @user.activate
    @user.create_reset_digest
  end

  scenario 'user changes the password' do
    visit api_v1_password_reset_path(
      token: @user.reset_token, email: @user.email, format: 'html')
    fill_in '新しいパスワード：', with: 'changed_password'
    fill_in '新しいパスワード再入力：', with: 'changed_password'
    click_button "パスワードを変更する"

    aggregate_failures do
      new_password_user = User.find_by(email: @user.email)
      expect(new_password_user.authenticate("changed_password")).to be_truthy
      expect(page).to have_current_path "/api/v1/password/reset/completed.html"
      expect(page).to have_content "パスワードを変更しました！"
      expect(mail).to deliver_to @user.email
      expect(mail).to have_subject(/【 KATSUOWEB 】パスワードを変更しました/)
    end
  end

  scenario 'user can not change the password due to blank' do
    visit api_v1_password_reset_path(
      token: @user.reset_token, email: @user.email, format: 'html')
    fill_in '新しいパスワード：', with: ''
    fill_in '新しいパスワード再入力：', with: 'changed_password'
    click_button "パスワードを変更する"

    aggregate_failures do
      new_password_user = User.find_by(email: @user.email)
      expect(new_password_user.authenticate("changed_password")).to be false
      expect(page).to have_current_path api_v1_password_reset_path(
        token: @user.reset_token, email: @user.email, format: 'html')
      expect(page).to have_content "パスワードが空白です。"
      expect(mail).to be nil
    end
  end

  scenario 'user can not change the password due to different between password and password_comfirmation' do
    visit api_v1_password_reset_path(
      token: @user.reset_token, email: @user.email, format: 'html')
    fill_in '新しいパスワード：', with: 'changed_password'
    fill_in '新しいパスワード再入力：', with: 'not_changed_password'
    click_button "パスワードを変更する"

    aggregate_failures do
      new_password_user = User.find_by(email: @user.email)
      expect(new_password_user.authenticate("changed_password")).to be false
      expect(page).to have_current_path api_v1_password_reset_path(
        token: @user.reset_token, email: @user.email, format: 'html')
      expect(page).to have_content "無効なパスワードです。"
      expect(mail).to be nil
    end
  end
end
