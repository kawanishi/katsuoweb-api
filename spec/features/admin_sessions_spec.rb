require 'rails_helper'

RSpec.feature "AdminSessions", type: :feature do
  before do
    @user = User.create(attributes_for(:user, :admin_user))
    @user.save!
    @user.activate
    create(:user)
  end

  scenario 'user can login as admin user' do
    @user.update_attributes(admin: true)
    visit admin_sign_in_path
    fill_in 'Email', with: @user.email
    fill_in 'Password', with: @user.password
    click_button 'ログイン'
    aggregate_failures do
      expect(page).to have_current_path admin_home_path
      expect(page).to have_content '管理者ホーム'
      expect(page).to have_link 'ユーザーマスター'
      expect(page).to have_link 'ページマスター'
    end
  end

  scenario 'user can not login if not admin user' do
    visit admin_sign_in_path
    fill_in 'Email', with: @user.email
    fill_in 'Password', with: @user.password
    click_button 'ログイン'
    aggregate_failures do
      expect(page).to have_current_path admin_sign_in_path
      expect(page).to have_content '管理ユーザーではありません。'
    end
  end

  scenario 'admin user can logout' do
    @user.update_attributes(admin: true)
    visit admin_sign_in_path
    fill_in 'Email', with: @user.email
    fill_in 'Password', with: @user.password
    click_button 'ログイン'
    click_link 'LOGOUT'
    aggregate_failures do
      expect(page).to have_current_path admin_sign_in_path
      expect(page).to have_content 'ログアウトしました。'
    end
  end
end
