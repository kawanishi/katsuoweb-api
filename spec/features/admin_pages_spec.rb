require 'rails_helper'

RSpec.feature "AdminPages", type: :feature do
  before do
    @user = create(:user, :admin_user)
    @user.save!
    @user.activate
    @user.update_attributes(admin: true)
  end

  scenario 'all pages are displayed' do
    page_attributes = attributes_for(:page)
    page_attributes[:user_id] = @user.id
    page_attributes[:title] = 'ページマスターテスト'
    Page.create(page_attributes)
    visit admin_sign_in_path
    fill_in 'Email', with: @user.email
    fill_in 'Password', with: @user.password
    click_button 'ログイン'
    click_link 'ページマスター'
    aggregate_failures do
      expect(page).to have_current_path admin_pages_path
      expect(page).to have_content 'ページマスターテスト'
      expect(page).to have_link '削除'
    end
  end

  scenario 'it should display warning message if any pages not found' do
    visit admin_sign_in_path
    fill_in 'Email', with: @user.email
    fill_in 'Password', with: @user.password
    click_button 'ログイン'
    click_link 'ページマスター'
    aggregate_failures do
      expect(page).to have_current_path admin_pages_path
      expect(page).to have_content 'ページが見つかりません。'
    end
  end

  scenario 'admin user can delete pages' do
    page_attributes = attributes_for(:page)
    page_attributes[:user_id] = @user.id
    page_attributes[:title] = 'ページマスターテスト'
    Page.create(page_attributes)
    visit admin_sign_in_path
    fill_in 'Email', with: @user.email
    fill_in 'Password', with: @user.password
    click_button 'ログイン'
    click_link 'ページマスター'
    click_link '削除', match: :first
    aggregate_failures do
      expect(page).to have_current_path admin_pages_path
      expect(page).to_not have_content 'ページマスターテスト'
    end
  end

  scenario 'error message should show if delete page fail' do
    page_attributes = attributes_for(:page)
    page_attributes[:user_id] = @user.id
    page_attributes[:title] = 'ページマスターテスト'
    Page.create(page_attributes)
    allow_any_instance_of(Page).to receive(:destroy).and_return(false)
    visit admin_sign_in_path
    fill_in 'Email', with: @user.email
    fill_in 'Password', with: @user.password
    click_button 'ログイン'
    click_link 'ページマスター'
    click_link '削除', match: :first
    aggregate_failures do
      expect(page).to have_current_path admin_pages_path
      expect(page).to have_content 'ページの削除に失敗しました。'
      expect(page).to have_content 'ページマスターテスト'
    end
  end
end
