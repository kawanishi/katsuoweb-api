Rails.application.routes.draw do
  root 'notice#release'
  get 'notice' => 'notice#release'

  namespace :api, defaults: {format: 'json'} do
    namespace :v1 do
      post 'user/sign_up' => 'users#create'
      get 'user/me' => 'users#show'
      patch 'user/me/profile' => 'users#update'
      get 'user/activate' => 'users#edit'
      get 'user/activate/completed' => 'users#completed'
      get 'user/all' => 'users#index'
      get 'user/other/:id' => 'users#other'
      post 'user/inquiry' => 'users#inquiry'
      post 'user/delete' => 'users#destroy'

      post 'sign_in' => 'sessions#create'
      delete 'sign_out' => 'sessions#destroy'

      post 'password/reset' => 'passwords#create'
      get 'password/reset' => 'passwords#edit'
      patch 'password/reset' => 'passwords#update'
      get 'password/reset/completed' => 'passwords#completed'

      resources :users do
        member do
          get :following, :followers
        end
      end

      resources :pages do
        post :search, on: :collection
        get :user_page, on: :member
        get :callback_auth, on: :collection
        patch :upload_video, on: :member
        get :timeline_page, on: :collection
        get :voted, on: :collection
        get :pickup, on: :collection
        get :headers, on: :member
      end

      resources :relationships, only: [:create, :destroy]

      resources :likes, only: [:create, :destroy]
    end
  end

  namespace :admin do
    get 'sign_in' => 'sessions#new'
    post 'sign_in' => 'sessions#create'
    delete 'sign_out' => 'sessions#destroy'
    get 'home' => 'home#index'
    resources :users, only: [:index, :destroy]
    resources :pages, only: [:index, :destroy]
  end
end
